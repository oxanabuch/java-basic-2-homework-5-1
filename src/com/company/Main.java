package com.company;

public class Main {

    public static void main(String[] args) {

        for (int i = 0; i <= 100; i++) {
            if (i == 13 || i == 66)
                continue;
            System.out.println(i);
        }
        System.out.println();

        int cycleResult = 1;

        for (int i = 1; i < 100; i++) {
            if (cycleResult > 1000) {
                System.out.println(cycleResult);
                cycleResult = 1001;
                break;
            }
            cycleResult *= i;
        }
    }
}
